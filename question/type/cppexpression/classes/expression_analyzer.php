<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines interface for analyser.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression;
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/question/type/cppexpression/analyser_errors.php');

/**
 * Interface for analyser classes.
*/
interface expression_analyzer {

    /**
     * Check: can analyser process $expression.
     *
     * @param $expression expression that is processed by analyser.
     * @return array of accepting_error objects, empty array for accepting.
     */
    public static function accept_expression($expression);

}
