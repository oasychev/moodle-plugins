<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines abstract class for analyser based hint.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace qtype_cppexpression;
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/question/type/poasquestion/hints.php');


/**
 * Abstract class of analyser.
 */
abstract class hint_from_analyzer extends qtype_specific_hint implements expression_analyzer {

    /** @var number mode (option) of analyser */
    public $mode=null;
    /** @var object Question object, created this hint*/
    public $question;
    /** @var string Hint key for this hint, useful for choosen miltiple instance hints*/
    public $hintkey;

    public function __construct($question, $hintkey, $mode = null) {
        $this->question = $question;
        $this->hintkey = $hintkey;
        if(!is_null($mode)) {
            $this->mode = $mode;
        }
    }

    abstract public static function accept_expression($answer);

    /**
     * Hint analyzer class names should be qtype_cppexpression\name()_hint .
     */
    abstract public function name();

    /**
     * Most cppexpression hints are single instance ones. Override if necessary.
     */
    public function hint_type() {
        return qtype_specific_hint::SINGLE_INSTANCE_HINT;
    }

    public function hint_description() {
        return get_string($this->name() . '_hint_description', 'qtype_cppexpression');
    }

    /**
     * Return an array of modes for given hint.
     *
     * Most hints well be content with these modes or their subset. Override if necessary.
     */
    public static function hint_modes() {
        return array(
            1 => get_string('cppexpr_hintstudent', 'qtype_cppexpression'),
            2 => get_string('cppexpr_hintanswer', 'qtype_cppexpression'),
            3 => get_string('cppexpr_hintboth', 'qtype_cppexpression')
        );
    }

    /**
     * Response-basedness of standard hint modes. Override if necessary.
     */
    public function hint_response_based() {
        if($this->mode==1 && $this->mode==3) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns whether response allows for the hint to be done.
     */
    public function hint_available($response = null) {
        return true;

        $isavailable = true;
        // For no response only not response based hints can be available.
        if(is_null($response) && $this->hint_response_based()) {
            $isavailable = false;
        }

        return $isavailable;
    }

    public function penalty_for_specific_hint($response = null) {
        $field = $this->name . 'penalty';
        return $this->question->$field;
    }

    abstract public function render_hint($renderer, question_attempt $qa = null, question_display_options $options = null, $response = null);
}