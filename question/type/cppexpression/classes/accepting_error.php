<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Accepting error class.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace qtype_cppexpression;
defined('MOODLE_INTERNAL') || die();

/**
 * Abstract class of analyzer error.
 */
class accepting_error extends error {

    /** @var string Internal name of the analyzer */
    public $analyzer;

    /**
     * Creates error object
     *
     * @param node block_formal_langs_ast_node child object Node that caused error.
     * @param message string "something" in "Analyzer doesn't support something" phrase.
     * @param expression string Expression containing error.
     */
    public function __construct($node, $message, $expression, $analyzer) {
        parent::__construct($node, $message, $expression);
        $this->analyzer = $analyzer;
        $a = new stdClass;
        $a->analyzer = get_string($analyzer, 'qtype_cppexpression');
        $a->errormsg = $this->errormsg;
        $this->errormsg = get_string('form_erroracceptfail', 'qtype_cppexpression', $a);
    }
}
