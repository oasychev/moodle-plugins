<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_cppexpression', language 'en'
 *
 * @package    qtype
 * @subpackage cppexpression
 * @copyright &copy; 2014 Oleg Sychev, Volgograd State Technical University 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addmoreanswerblanks'] = 'Blanks for {no} More Answers';
$string['answer'] = 'Answer: {$a}';
$string['answermustbegiven'] = 'You must enter an answer if there is a grade or feedback.';
$string['answerno'] = 'Answer {$a}';
$string['caseno'] = 'No, case is unimportant';
$string['casesensitive'] = 'Case sensitivity';
$string['caseyes'] = 'Yes, case must match';
$string['correctansweris'] = 'The correct answer is: {$a}';
$string['correctanswers'] = 'Correct answers';
$string['filloutoneanswer'] = 'You must provide at least one possible answer. Answers left blank will not be used. \'*\' can be used as a wildcard to match any characters. The first matching answer will be used to determine the score and feedback.';
$string['maxerrorsshowndescription'] = 'Maximum number of errors shown for each expression in the question editing form';
$string['maxerrorsshownlabel'] = 'Maximum number of errors shown';
$string['notenoughanswers'] = 'This type of question requires at least {$a} answers';
$string['pleaseenterananswer'] = 'Please enter an answer.';
$string['pluginname'] = 'Cppexpression';
$string['pluginname_help'] = 'Cppexpression help'; // TODO rewrite
$string['pluginname_link'] = 'question/type/cppexpression';
$string['pluginnameadding'] = 'Adding a C++ expression question';
$string['pluginnameediting'] = 'Editing a C++ expression question';
$string['pluginnamesummary'] = 'Allows a response C++ expression that is graded by different analyzers.';
$string['questioneditingheading'] = 'Question editing settings';

// Hinting options.
$string['cppexpr_hintno'] = 'Don\'t show' ;
$string['cppexpr_hintstudent'] = 'Show for the student\'s response';
$string['cppexpr_hintanswer'] = 'Show for the correct answer';
$string['cppexpr_hintboth'] = 'Show for both answer and response';
$string['cppexpr_hintcalculatedresult'] = 'Show calculated result';

// Grading options.
//$string['cppexpr_gradewithshowingerrors'] = 'Use with showing errors' ;
//$string['cppexpr_gradeonly'] = 'Only grade' ;

// Question editing form.
$string['form_answerinstruct'] = 'Fill out answers as C++ expressions. You must enter at least one answer with 100% grade.';
$string['form_declarations'] = 'Declarations of variables';
$string['form_gradinganalyzer'] = 'Grading engine';
$string['form_gradeborder'] = 'Hint grade border';
$string['form_penalty'] = 'Penalty for {$a} hint';
$string['form_analyzersheader'] = 'Hinting';
$string['form_answer'] = 'C++ expression';

// Error messages.
$string['form_erroracceptfail'] = '{$a->analyzer} analyzer don\'t support {$a->errormsg}.';
$string['form_errorhintgradeborder'] = 'Hint grade border must be a number from 0 to 1.';
$string['form_errorpenalty'] = 'Hint grade border must be a number from 0 to 1.';
$string['form_toomanyerrors'] = '.......{$a} more errors';

// Array2d analyzers.
$string['array2d_hint'] = 'Filling 2D array hint';
$string['array2d_grader'] = 'Filling 2D array grading';