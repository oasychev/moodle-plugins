<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cppexpression question type version information.
 *
 * @package     cppexpression
 * @author      Sychev Oleg
 * @copyright   &copy; 2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

$plugin->component = 'qtype_cppexpression';

$plugin->version   = 2014112500;
$plugin->requires  = 2013050100;

$plugin->maturity  = MATURITY_STABLE;

$plugin->dependencies = array(
    'qtype_shortanswer' => 2013050100,
    'qbehaviour_adaptivehints' => 2014071000,
    'qbehaviour_adaptivehintsnopenalties' => 2014071000,
    'qbehaviour_interactivehints' => 2014071000,
    'qtype_poasquestion' => 2014071000,
    'block_formal_langs' => 2014071000
);