<?php
// This file is part of Poas Assignment plugin - https://bitbucket.org/oasychev/moodle-plugins/
//
// Poas Assignment plugin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Poas Assignment plugin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Poas Assignment plugin.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines unit-tests for "Graders" subplugins PluginInfo classes
 *
 * @copyright &copy; 2011 Oleg Sychev
 * @author Alexander Lyashenko, Volgograd State Technical University
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_poasassignment
 */
use mod_poasassignment\plugininfo\poasassignment as poasassignment;

global $CFG;
require_once($CFG->dirroot.'/mod/poasassignment/classes/plugininfo/poasassignment.php');

/**
 * Tests deleting "Graders" subplugins
 */
class poasassignment_plugininfo_test extends \advanced_testcase {

    public function test_clearing_autotester_db() {
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignment();
        $testclass->name = "autotester";
        $pluginid = $DB->get_field('poasassignment_graders', 'id', array('name' => 'autotester'));

        $testRecordsCount = 4;
        $atResTableData = array();
        $atTableData = array();
        $usedGradersTableData = array();

        for ($index = 0; $index < $testRecordsCount; $index++) {
            $record_to_at_res = new stdClass();
            $record_to_at_res->id = $index;
            $record_to_at_res->testid = $testRecordsCount - $index;
            $record_to_at_res->attemptid = 0;
            $record_to_at_res->studentout = 'Student';
            $record_to_at_res->testpassed = $index % 2;
            array_push($atResTableData, $record_to_at_res);

            $record_to_autotester = new stdClass();
            $record_to_autotester->id = $index;
            $record_to_autotester->taskid = rand(0, $index);
            $record_to_autotester->questionid = rand(0, $index);
            array_push($atTableData, $record_to_autotester);

            $record_to_used_graders = new stdClass();
            $record_to_used_graders->id = $index;
            $record_to_used_graders->poasassignmentid = $index;
            $record_to_used_graders->graderid = $pluginid;
            array_push($usedGradersTableData, $record_to_used_graders);
        }

        $DB->insert_records('poasassignment_gr_at_res', $atResTableData);
        $DB->insert_records('poasassignment_gr_autotester', $atTableData);
        $DB->insert_records('poasassignment_used_graders', $usedGradersTableData);

        $testclass->uninstall_cleanup();

        $this->assertCount(0,$DB->get_records('poasassignment_gr_at_res'));
        $this->assertCount(0,$DB->get_records('poasassignment_gr_autotester'));
        $this->assertCount(0, $DB->get_records('poasassignment_used_graders', array('graderid' => $pluginid)));
    }

    public function test_clearing_remote_autotester_db() {
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignment();
        $testclass->name = "autotester";
        $pluginid = $DB->get_field('poasassignment_graders', 'id', array('name' => 'remote_autotester'));

        $testRecordsCount = 4;
        $raTestsTableData = array();
        $raTableData = array();
        $usedGradersTableData = array();

        for ($index = 0; $index < $testRecordsCount; $index++) {
            $record_to_ra_tests = new stdClass();
            $record_to_ra_tests->id = $index;
            $record_to_ra_tests->test = "Test $index";
            $record_to_ra_tests->timetested = 0;
            $record_to_ra_tests->testin = "Test $index";
            $record_to_ra_tests->testout = "Result $index";
            $record_to_ra_tests->studentout = "Student $index";
            $record_to_ra_tests->testpassed = $index % 2;
            array_push($raTestsTableData, $record_to_ra_tests);

            $record_to_ra = new stdClass();
            $record_to_ra->id = $index;
            $record_to_ra->attemptid = rand(0, $index);
            $record_to_ra->timecreated = rand(0, $index);
            $record_to_ra->serverresponse = '200';
            $record_to_ra->timecompilestarted = 0;
            $record_to_ra->timecompiled = 0;
            $record_to_ra->compiled = $index % 2;
            $record_to_ra->timeteststart = $index %2;
            $record_to_ra->compilemessage = "Compiled $index";
            $record_to_ra->testsfound = $index %2;
            $record_to_ra->result = ($index + 1) % 2;
            $record_to_ra->timeclosed = 0;
            array_push($raTableData, $record_to_ra);

            $record_to_used_graders = new stdClass();
            $record_to_used_graders->id = $index;
            $record_to_used_graders->poasassignmentid = $index;
            $record_to_used_graders->graderid = $pluginid;
            array_push($usedGradersTableData, $record_to_used_graders);
        }

        $testclass->uninstall_cleanup();

        $this->assertCount(0,$DB->get_records('poasassignment_gr_ra'));
        $this->assertCount(0,$DB->get_records('poasassignment_gr_ra_tests'));
        $this->assertCount(0, $DB->get_records('poasassignment_used_graders', array('graderid' => $pluginid)));
    }

    public function test_clearing_simple_grader_db(){
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignment();
        $testclass->name = "simple_grader";
        $pluginid = $DB->get_field('poasassignment_graders', 'id', array('name' => 'simple_grader'));

        $testRecordsCount = 4;
        $usedGradersTableData = array();

        for ($index = 0; $index < $testRecordsCount; $index++) {
            $record_to_used_graders = new stdClass();
            $record_to_used_graders->id = $index;
            $record_to_used_graders->poasassignmentid = $index;
            $record_to_used_graders->graderid = $pluginid;
            array_push($usedGradersTableData, $record_to_used_graders);
        }

        $DB->insert_records('poasassignment_used_graders', $usedGradersTableData);

        $testclass->uninstall_cleanup();

        $this->assertCount(0, $DB->get_records('poasassignment_used_graders', array('graderid' => $pluginid)));
    }
}

