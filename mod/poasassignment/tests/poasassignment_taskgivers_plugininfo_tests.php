<?php
// This file is part of Poas Assignment plugin - https://bitbucket.org/oasychev/moodle-plugins/
//
// Poas Assignment plugin is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Poas Assignment plugin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Poas Assignment plugin.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Defines unit-tests for "Task Givers" subplugins PluginInfo classes
 *
 * @copyright &copy; 2011 Oleg Sychev
 * @author Alexander Lyashenko, Volgograd State Technical University
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package mod_poasassignment
 */
use mod_poasassignment\plugininfo\poasassignmenttaskgivers as poasassignmenttaskgivers;

global $CFG;
require_once($CFG->dirroot.'/mod/poasassignment/classes/plugininfo/poasassignment.php');

/**
 * Tests deleting "Task Givers" subplugins
 */
class poasassignment_taskgivers_plugininfo_tests extends \advanced_testcase {
    /**
     * Filling tables that used by Analogy Choise sub-plugin, then deleting Analogy Choise
     */
    public function test_clearing_analogychoise(){
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignmenttaskgivers();
        $testclass->name = "analogy_choice";
        $pluginid = $DB->get_field('poasassignment_taskgivers', 'id', array('name' => 'analogy_choice'));

        $testRecordsCount = 4;
        $testAssignments = array();
        $testAnalogyRecords = array();

        for ($index = 0; $index < $testRecordsCount; $index++) {
            $assignment = new stdClass();
            $assignment->course = 0;
            $assignment->name = "Assignment $index";
            $assignment->intro = "Assignment $index intro";
            $assignment->introformat = 0;
            $assignment->timemodified = 0;
            $assignment->availabledate = 0;
            $assignment->choicedate = 0;
            $assignment->deadline = 0;
            $assignment->flags = $index % 2 ? 1 << 6 : 0;
            $assignment->taskgiverid = $pluginid;
            $assignment->uniqueness = 0;
            $assignment->penalty = 0.05;
            $assignment->grade = 100;
            array_push($testAssignments, $assignment);

            $analogyRecord = new stdClass();
            $analogyRecord->originalid = rand(0, $index);
            $analogyRecord->additionalid =  $analogyRecord->originalid + 1;
            array_push($testAnalogyRecords, $analogyRecord);
        }

        $DB->insert_records('poasassignment', $testAssignments);
        $DB->insert_records('poasassignment_analogych', $testAnalogyRecords);

        $testclass->uninstall_cleanup();

        $this->assertCount(0,$DB->get_records('poasassignment_analogych'));
        $this->assertCount(0, $DB->get_records_select('poasassignment', "flags & 64 AND taskgiverid = $pluginid"));
        $this->assertCount(0, $DB->get_records('poasassignment_taskgivers', array('id' => $pluginid)));
    }

    /**
     * Filling tables that used by Parameter Choise sub-plugin, then deleting Parameter Choise
     */
    public function test_clearing_parameterchoise(){
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignmenttaskgivers();
        $testclass->name = "parameterchoice";
        $pluginid = $DB->get_field('poasassignment_taskgivers', 'id', array('name' => 'parameterchoice'));

        $testRecordsCount = 4;
        $testAssignments = array();
        $testParamRecords = array();

        for ($index = 0; $index < $testRecordsCount; $index++) {
            $assignment = new stdClass();
            $assignment->course = 0;
            $assignment->name = "Assignment $index";
            $assignment->intro = "Assignment $index intro";
            $assignment->introformat = 0;
            $assignment->timemodified = 0;
            $assignment->availabledate = 0;
            $assignment->choicedate = 0;
            $assignment->deadline = 0;
            $assignment->flags = $index % 2 ? 1 << 6 : 0;
            $assignment->taskgiverid = $pluginid;
            $assignment->uniqueness = 0;
            $assignment->penalty = 0.05;
            $assignment->grade = 100;
            array_push($testAssignments, $assignment);

            $paramRecord = new stdClass();
            $paramRecord->fieldid =  rand(0, $index);
            array_push($testParamRecords, $paramRecord);
        }

        $DB->insert_records('poasassignment', $testAssignments);
        $DB->insert_records('poasassignment_paramch', $testParamRecords);

        $testclass->uninstall_cleanup();

        $this->assertCount(0,$DB->get_records('poasassignment_paramch'));
        $this->assertCount(0, $DB->get_records_select('poasassignment', "flags & 64 AND taskgiverid = $pluginid"));
        $this->assertCount(0, $DB->get_records('poasassignment_taskgivers', array('id' => $pluginid)));
    }

    /**
     * Filling tables that used by Student Choise sub-plugin, then deleting Student Choise
     */
    public function test_clearing_studentchoise(){
        global $DB;
        $this->resetAfterTest(true);

        $testclass = new poasassignmenttaskgivers();
        $testclass->name = "studentschoice";
        $pluginid = $DB->get_field('poasassignment_taskgivers', 'id', array('name' => 'studentschoice'));

        $testRecordsCount = 4;
        $testAssignments = array();
        $testParamRecords = array();

        for ($index = 0; $index < $testRecordsCount; $index++) {
            $assignment = new stdClass();
            $assignment->course = 0;
            $assignment->name = "Assignment $index";
            $assignment->intro = "Assignment $index intro";
            $assignment->introformat = 0;
            $assignment->timemodified = 0;
            $assignment->availabledate = 0;
            $assignment->choicedate = 0;
            $assignment->deadline = 0;
            $assignment->flags =  1 << 6;
            $assignment->taskgiverid = $pluginid;
            $assignment->uniqueness = 0;
            $assignment->penalty = 0.05;
            $assignment->grade = 100;
            array_push($testAssignments, $assignment);
        }

        $DB->insert_records('poasassignment', $testAssignments);

        $testclass->uninstall_cleanup();

        $this->assertCount(0, $DB->get_records_select('poasassignment', "flags & 64 AND taskgiverid = $pluginid"));
        $this->assertCount(0, $DB->get_records('poasassignment_taskgivers', array('id' => $pluginid)));
    }
}
