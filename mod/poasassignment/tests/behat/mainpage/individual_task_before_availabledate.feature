@mod @mod_poasassignment @core_role @core_access
Feature: In main view of poasassignment, student cannot see poasassignment in individual mode before available date
  In order to see main view of poasassignment in individual mode
  As a student
  I need see message that time has not come yet

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name             | description    | beforeavailabledate |activateindividualtasks | taskgiverid | uniqueness |answertext |
      | poasassignment | C1     | pas1     | Poasassignment 1 | Test task intro| 1                   | 1                      | 3           |0           | 1         |

  # Overview
  #
  # Found bugs:
  # 1) Cannot create multiple scenarios with answer text. The first scenario execute successfully, then in the next scenarios
  # do not have to insert the text field response
  # 2) Cannot create multiple instance of poasassignment module
  #
  # Test success:
  # -
  #
  # Test fail:
  # 1) Scenario: Student cannot see poasassignment in individual mode before available date

  @javascript
  Scenario: Student cannot see poasassignment in individual mode before available date
    Given I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And I click on "Tasks" "link" in the "Navigation" "block"
    And I press "Create task"
    And I set the field "name" to "task1"
    And I press "Save changes"
    And I press "Create task"
    And I set the field "name" to "task2"
    And I press "Save changes"
    And I log out
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    # Throw exception instead message on a page. How check it?
    Then I should see "This module isn't opened yet"
