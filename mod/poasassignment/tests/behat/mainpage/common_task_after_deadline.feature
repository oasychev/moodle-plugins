@mod @mod_poasassignment @core_role @core_access
Feature: In main view of poasassignment, student can add submission for common task after deadline if it's not restrict
  In order to add submission for common task in main view of poasassignment
  As a student
  I can add submission for common task

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name             | description    | afterduedate |answertext |
      | poasassignment | C1     | pas1     | Poasassignment 1 | Test task intro| 1            | 1         |
  # Overview
  #
  # Found bugs:
  # 1) Cannot create multiple scenarios with answer text. The first scenario execute successfully, then in the next scenarios
  # do not have to insert the text field response
  # 2) Cannot create multiple instance of poasassignment module
  #
  # Test success:
  # -
  #
  # Test fail:
  # 1) Scenario: Student can add submission for common task after deadline if it's not restrict

  @javascript
  Scenario: Student can add submission for common task after deadline if it's not restrict
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Poasassignment 1"
    And "Add submission" "button" should exist
    # Wait when come due date
    And I wait "5" seconds
    And I press the "reload" button in the browser
    Then "Add submission" "button" should exist
