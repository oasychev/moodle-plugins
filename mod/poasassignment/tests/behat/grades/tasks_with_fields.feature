@mod @mod_poasassignment @core_role @core_access
Feature: Grades page testing
  Create course, create poasassigment, grades comments ets.

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name                     | description    | activateindividualtasks | taskgiverid | secondchoice | uniqueness |answertext |availabledate | choicedate | deadline | severalattempts | penalty | finalattempts |
      | poasassignment | C1     | pas1     | Test poasassignment name | Test task intro| 1                       | 4           |1             |0           | 1         |0             | 0          | 0        | 1               | 0       | 1             |

  #@javascript
  #Scenario: Creator of course can see visible attempt of students
  #  When I log in as "creator"
  #  And I am on site homepage
  #  And I follow "Course 1"
  #  Then I should not see "Test poasassignment name"
  #  And I log out

  @javascript
  Scenario: Teacher of course grades student attempts
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should see "0 of 2 need grade"
    Then I should see "Task's fields"

    And I follow "Task's fields"

    And I click on "Add task field" "button"
    And I set the field "Field name" to "Test field 1"
    And I click on "Save changes" "button"
    Then I should see "Are you sure you want to create the field?"
    And I click on "Yes" "button"
    Then I should see "Test field 1"

    And I click on "Add task field" "button"
    And I set the field "Field name" to "Test field 2"
    And I click on "Save changes" "button"
    Then I should see "Are you sure you want to create the field?"
    And I click on "Yes" "button"
    Then I should see "Test field 2"

    And I follow "Tasks"

    And I click on "Create task" "button"
    And I set the field "Task name" to "Test task 1"
    And I click on "Save changes" "button"

    And I click on "Create task" "button"
    And I set the field "Task name" to "Test task 2"
    And I click on "Save changes" "button"

    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should not see "0 of 2 need grade"
    Then I should not see "No attempts have been made on this assignment"
    Then I should see "Click here to take task"
    And I follow "Click here to take task"

    And I log out