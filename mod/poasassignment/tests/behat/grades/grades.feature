@mod @mod_poasassignment @core_role @core_access
Feature: Grades page testing
  Create course, create poasassigment, grades comments ets.

  Background:
    Given the following "courses" exist:
      | fullname | shortname | category | groupmode |
      | Course 1 | C1        | 0        | 1         |
    And the following "users" exist:
      | username        | firstname | lastname   | email                 |
      | manager         | Manager   | M          | manager@example.com   |
      | creator         | Creator   | C          | creator@example.com   |
      | teacher         | Teacher   | T          | teacher@example.com   |
      | student1        | Student   | S1         | student1@example.com  |
      | student2        | Student   | S2         | student2@example.com  |
    And the following "course enrolments" exist:
      | user          | course | role           |
      | creator       | C1     | coursecreator  |
      | teacher       | C1     | editingteacher |
      | student1      | C1     | student        |
      | student2      | C1     | student        |
    And the following "role assigns" exist:
      | user     | role    | contextlevel | reference |
      | manager  | manager | System       |           |
    Given the following "activities" exist:
      | activity       | course | idnumber | name                     | description    | activateindividualtasks | taskgiverid | secondchoice | uniqueness |answertext |availabledate | choicedate | deadline | severalattempts | penalty | finalattempts |
      | poasassignment | C1     | pas1     | Test poasassignment name | Test task intro| 1                       | 4           |1             |0           | 1         |0             | 0          | 0        | 1               | 0       | 1             |

  #@javascript
  #Scenario: Creator of course can see visible attempt of students
  #  When I log in as "creator"
  #  And I am on site homepage
  #  And I follow "Course 1"
  #  Then I should not see "Test poasassignment name"
  #  And I log out

  @javascript
  Scenario: Teacher of course can see visible attempt of students
    #Teacher
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should see "need grade"
    Then I should see "Task's fields"

    #Create 2 tasks
    And I follow "Tasks"

    And I click on "Create task" "button"
    And I set the field "Task name" to "Test task 1"
    And I click on "Save changes" "button"

    And I click on "Create task" "button"
    And I set the field "Task name" to "Test task 2"
    And I click on "Save changes" "button"

    And I log out

    #Student 1
    #student should see two created tests
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should not see "need grade"
    Then I should not see "No attempts have been made on this assignment"
    Then I should see "Click here to take task"
    And I follow "Click here to take task"
    Then I should see "Test task 1"
    Then I should see "Test task 2"

    #Student take Test task 1
    And I click on "//td[a/text()='Test task 1']//img[@title='Take this task']" "xpath_element"
    And I click on "Yes" "button"
    Then I should see "Task was taken"

    #Student send answer
    And I click on "Add submission" "button"
    And I set the field "Enter your submission here" to "Test answer 1"
    And I click on "Draft" "checkbox"
    And I click on "Send submission" "button"
    And I log out

     #Teacher add grades and comments
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    And I follow "Submissions"
    Then I should see "Draft" in the "//tr[td/a/text()='Test task 1']" "xpath_element"
    And I log out

    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then "Edit submission" "button" should exist
    And I click on "Edit submission" "button"
    And I set the field "Enter your submission here" to "Test answer 1 edited"
    And I click on "Send submission" "button"

    And I log out

    #Student 2
    #student should see two created tests
    When I log in as "student2"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should not see "need grade"
    Then I should not see "No attempts have been made on this assignment"
    Then I should see "Click here to take task"
    And I follow "Click here to take task"
    Then I should see "Test task 1"
    Then I should see "Test task 2"

    #Student take Test task 2
    And I click on "//td[a/text()='Test task 2']//img[@title='Take this task']" "xpath_element"
    And I click on "Yes" "button"

    #Student send answer with finilize
    And I click on "Add submission" "button"
    And I set the field "Enter your submission here" to "Test answer 2"
    And I click on "I am sure in my answer, finilize" "checkbox"
    And I click on "Send submission" "button"
    Then "Edit submission" "button" should not exist

    And I log out

    #Teacher add grades and comments
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    And I follow "Submissions"
    Then I should see "Task completed" in the "//tr[td/a/text()='Test task 1']" "xpath_element"

    And I click on "Add grade" "link" in the "//tr[td/a/text()='Test task 1']" "xpath_element"
    Then "Grade" "select" should be visible
    And I set the field "grade" to "95"
    And I click on "Save changes" "button"
    Then I should see "Task completed" in the "//tr[td/a/text()='Test task 1']" "xpath_element"
    And I should see "95" in the "//tr[td/a/text()='Test task 1']" "xpath_element"
    And I should see "Edit grade" in the "//tr[td/a/text()='Test task 1']" "xpath_element"

    And I click on "Edit grade" "link" in the "//tr[td/a/text()='Test task 1']" "xpath_element"
    Then "Grade" "select" should be visible
    And I set the field "grade" to "100"
    And I click on "Finalize grade" "checkbox"
    And I set the field with xpath "//div[@class='comment-area']//textarea" to "excellent"
    And I click on "Save changes" "button"

    And I click on "Add grade" "link" in the "//tr[td/a/text()='Test task 2']" "xpath_element"
    Then "Grade" "select" should be visible
    And I set the field "grade" to "50"
    And I set the field with xpath "//div[@class='comment-area']//textarea" to "bad answer"
    And I click on "Save changes" "button"
    Then I should see "Task completed" in the "//tr[td/a/text()='Test task 1']" "xpath_element"
    And I should see "50" in the "//tr[td/a/text()='Test task 2']" "xpath_element"
    And I should see "Edit grade" in the "//tr[td/a/text()='Test task 2']" "xpath_element"

    And I log out

    #Student 1
    #Student 1 see his grade and comments and answer the comments
    When I log in as "student1"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should not see "need grade"
    And I should not see "No attempts have been made on this assignment"
    And I should not see "Click here to take task"
    And I should see "Your task is"
    And I should see "Test task 1"
    And I should see "Test answer 1 edited"
    And I should see "Test answer 1"
    Then "Edit submission" "button" should not exist
    And I click on "Comments (2)" "link"
    And I should see "excellent"
    And I set the field with xpath "//div[@class='comment-area']//textarea" to "thanks"
    Then I click on "Save comment" "link"

    And I log out

    #Teacher answer on student comment
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    And I follow "Attempts"
    Then I set the field "Student" to "Student S1"
    And I click on "go" "button"
    Then I click on "Comments (3)" "link"
    And I should see "thanks"
    And I set the field with xpath "//div[@class='comment-area']//textarea" to "welcome"
    Then I click on "Save comment" "link"

    And I log out

    #Student 2 see comment and edit his answer
    When I log in as "student2"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then I should not see "need grade"
    And I should not see "No attempts have been made on this assignment"
    And I should not see "Click here to take task"
    And I should see "Your task is"
    And I should see "Test task 2"
    And I should see "Test answer 2"
    Then I click on "Comments (1)" "link"
    And I should see "bad answer"
    And I set the field with xpath "//div[@class='comment-area']//textarea" to ":("
    Then I click on "Save comment" "link"
    Then "Edit submission" "button" should exist
    And I click on "Edit submission" "button"
    And I set the field "Enter your submission here" to "Test answer 2 edited"
    And I click on "I am sure in my answer, finilize" "checkbox"
    And I click on "Send submission" "button"

    Then I click on "Comments (2)" "link"
    And I should see "bad answer"
    And I should see ":("

    And I log out

     #Teacher show edited student 2 answer and edit his grade
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    And I follow "Submissions"
    Then I should see "Task completed" in the "//tr[td/a/text()='Test task 2']" "xpath_element"
    And I click on "Edit grade" "link" in the "//tr[td/a/text()='Test task 2']" "xpath_element"
    Then "Grade" "select" should be visible
    Then I click on "Previous attempts" "link"
    And I should see "bad answer"
    And I should see ":("
    And I set the field "grade" to "85"
    And I click on "Finalize grade" "checkbox"
    And I click on "Save changes" "button"
    Then I should see "85" in the "//tr[td/a/text()='Test task 2']" "xpath_element"

    And I log out

    When I log in as "student2"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    Then "Edit submission" "button" should not exist

    And I log out

    #Teacher check attempts comments
    When I log in as "teacher"
    And I am on site homepage
    And I follow "Course 1"
    And I follow "Test poasassignment name"
    And I follow "Attempts"
    Then I set the field "Student" to "Student S2"
    And I click on "go" "button"
    Then I click on "Comments (2)" "link"
    And I should see "bad answer"
    And I should see ":("

    And I log out