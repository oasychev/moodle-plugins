<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * POAS Assignment grader subplugin info class.
 *
 * @package    mod_poasassignment
 * @author     Nikita Kalinin <nixorv@gmail.com>
 * @copyright  2014 Oleg Sychev (Volgograd State Technical University) <oasychev@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_poasassignment\plugininfo;

use core\plugininfo\base, core_plugin_manager, moodle_url;

defined('MOODLE_INTERNAL') || die();


class poasassignment extends base {

    public function is_uninstall_allowed() {
        return true;
    }

    public function uninstall_cleanup() {
        global $DB;

        $pluginid = $DB->get_field('poasassignment_graders', 'id', array('name' => $this->name));

        if ( $this->name == "autotester") {
            $DB->delete_records('poasassignment_gr_at_res');
            $DB->delete_records('poasassignment_gr_autotester');
        } else if ( $this->name == "remote_autotester") {
            $DB->delete_records('poasassignment_gr_ra');
            $DB->delete_records('poasassignment_gr_ra_tests');
        }

        $DB->delete_records('poasassignment_used_graders', array('graderid' => $pluginid));

        parent::uninstall_cleanup();
    }
}