<?php
global $CFG;
require_once('abstract_page.php');
require_once(dirname(dirname(__FILE__)) . '/model.php');

class attempts_page extends abstract_page {
    /**
     * Assignee id.
     * @var int
     */
    private $assigneeid;

    /**
     * Group id.
     * @var int
     */
    private $groupid;

    public function __construct() {
        $this->assigneeid = optional_param('assigneeid', 0, PARAM_INT);
        $this->groupid = optional_param('group', 0, PARAM_INT);
    }

    function has_satisfying_parameters() {
        global $DB,$USER;
        $model = poasassignment_model::get_instance();
        $context = $model->get_context();
        $poasassignmentid = $model->get_poasassignment()->id;
        if ($this->assigneeid > 0) {
            if (! $this->assignee = $DB->get_record('poasassignment_assignee', array('id' => $this->assigneeid))) {
                $this->lasterror = 'errornonexistentassignee';
                return false;
            }
        }
        else {
            $poasassignmentid = $model->get_poasassignment()->id;
            $this->assignee = $model->get_assignee($USER->id, $poasassignmentid);
        }
        // Page exists always for teachers
        if (has_capability('mod/poasassignment:grade', $context) || has_capability('mod/poasassignment:finalgrades', $context)) {
            return true;
        }
        // Page exists, if assignee has attempts
        if ($this->assignee && $model->count_attempts($this->assignee->id) > 0) {
            // Page content is available if assignee wants to see his own attempts
            // or teacher wants to see them
            if($this->assignee->userid == $USER->id) {
                //if (has_capability('mod/poasassignment:viewownsubmission', $context)) {
                    return true;
                //}
                //else {
                //    $this->lasterror = 'errorviewownsubmissioncap';
                //    return false;
                //}
            }
            else {
                $this->lasterror = 'erroranothersattempts';
                return false;
            }
        }
        else {
            $this->lasterror = 'errorassigneenoattempts';
            return false;
        }
        return true;
    }

    public function view_assignee_block() {
        global $CFG;

        $poasmodel = poasassignment_model::get_instance();
        $cm = $poasmodel->get_cm();

        if (has_capability('mod/poasassignment:grade', $poasmodel->get_context())) {
            $mform = new assignee_choose_form(null, [
                'id' => $cm->id,
                'groupid' => $this->groupid,
            ]);
            $mform->display();

            groups_print_activity_menu($cm, $CFG->wwwroot . '/mod/poasassignment/view.php?id=' . $cm->id . '&page=attempts');
        }
    }

    public function view() {
        global $DB, $OUTPUT;
        $poasmodel = poasassignment_model::get_instance();
        $poasassignmentid = $poasmodel->get_poasassignment()->id;

        $this->view_assignee_block();

        // teacher has access to the page even if he has no task or attempts
        if(isset($this->assignee->id)) {
            $attempts = array_reverse($DB->get_records('poasassignment_attempts',
                                                       array('assigneeid'=>$this->assignee->id), 
                                                       'attemptnumber'));
            $plugins = $poasmodel->get_plugins();
            $latestattempt = $poasmodel->get_last_attempt($this->assignee->id);
            $attemptscount = count($attempts);  
            foreach($attempts as $attempt) {
                echo $OUTPUT->box_start();
                $hascap = has_capability('mod/poasassignment:viewownsubmission', $poasmodel->get_context());
                echo attempts_page::show_attempt($attempt, $hascap);
                // show disablepenalty/enablepenalty button
                if(has_capability('mod/poasassignment:grade',$poasmodel->get_context())) {
                    $cmid = $poasmodel->get_cm()->id;
                    if(isset($attempt->disablepenalty) && $attempt->disablepenalty==1) {
                        echo $OUTPUT->single_button(new moodle_url('warning.php?id='.$cmid.'&action=enablepenalty&attemptid='.$attempt->id), 
                                                                get_string('enablepenalty','poasassignment'));
                    }
                    else {
                        echo $OUTPUT->single_button(new moodle_url('warning.php?id='.$cmid.'&action=disablepenalty&attemptid='.$attempt->id), 
                                                                get_string('disablepenalty','poasassignment'));
                    }
                }
                $canseecriteriondescr = has_capability('mod/poasassignment:seecriteriondescription', $poasmodel->get_context());
                attempts_page::show_feedback($attempt, $latestattempt, $canseecriteriondescr);
                echo $OUTPUT->box_end();
                echo '<br>';
            }
        }
    }
    public static function use_echo() {
        return false;
    }
    public static function show_attempt($attempt, $showcontent = true) {
        $poasmodel = poasassignment_model::get_instance();
        $html = '';
        $html .= '<table class="poasassignment-table" align="center">';

        $values = array(
                        'attemptnumber' => $attempt->attemptnumber,
                        'attemptdate' => userdate($attempt->attemptdate),
                        'draft' => $attempt->draft == 1 ? get_string('yes') : get_string('no'),
                        'attemptisfinal' => $attempt->final == 1 ? get_string('yes') : get_string('no'),
                        'attempthaspenalty' => $attempt->disablepenalty == 1 ? get_string('no') : get_string('yes'),
                        'attempttotalpenalty' => $poasmodel->get_penalty($attempt->id));
        // hide penalty info if penalty is zero
        if ($values['attempttotalpenalty'] == 0) {
            unset($values['attempttotalpenalty']);
            unset($values['attempthaspenalty']);
        }

        foreach($values as $key => $value) {
            $html .= '<tr>';
            $html .= '<td class="header" >' . get_string($key,'poasassignment') . '</td>';
            if ($key == 'attempthaspenalty' || $key == 'attempttotalpenalty') {
                $html .= '<td class="critical" style="text-align:center;">' . $value . '</td>';
            }
            else {
                $html .= '<td style="text-align:center;">' . $value . '</td>';
            }
            $html .= '</tr>';
        }
        if ($showcontent) {
            $poasmodel = poasassignment_model::get_instance();
            $plugins = $poasmodel->get_plugins();
            $attemptcontent = '';
            foreach($plugins as $plugin) {
                require_once($plugin->path);
                $poasassignmentplugin = new $plugin->name();
                $attemptcontent .= $poasassignmentplugin->show_assignee_answer($attempt->assigneeid, $poasmodel->get_poasassignment()->id, 0, $attempt->id);
            }
            $html .= '<tr>';
            $html .= '<td colspan="2">' . $attemptcontent . '</td>';
            $html .= '</tr>';
        }

        $html .= '</table>';
        return $html;
    }
    public static function show_feedback($attempt, $latestattempt, $showdescription) {
        global $DB, $OUTPUT, $PAGE;
        $poasmodel = poasassignment_model::get_instance();
        $context = $poasmodel->get_context();

        echo get_string('totalratingis', 'poasassignment') . ': ' . $attempt->rating;

        $gradecontroller = get_grading_manager($poasmodel->get_context(), 'mod_poasassignment', 'submissions')->get_active_controller();
        if ($gradecontroller) {
            $grademenu = make_grades_menu($poasmodel->poasassignment->grade);
            $gradecontroller->set_grade_range($grademenu);
            $cangrade = has_capability('mod/assign:grade', $poasmodel->get_context());
            $gradefordisplay = $gradecontroller->render_grade($PAGE, $attempt->id, array(), '', $cangrade);
            if ($gradefordisplay != '') {
                echo $gradefordisplay;
            }
        }

        $options = new stdClass();
        $options->area = 'poasassignment_comment';
        $options->component = 'mod_poasassignment';
        $options->pluginname = 'poasassignment';
        $options->context = $context;
        $options->showcount = true;
        $options->itemid  = $attempt->id;
        $comment = new comment($options);
        echo $comment->output(true);
    }

    /**
     * Returns HTML to display comments on attempt.
     * @param stdObject $comments_itemid Poasassignment rating value id
     * @return string or null if no comments
     */
    public static function show_comments($comments_itemid) {
        $poasmodel = poasassignment_model::get_instance();
        $context = $poasmodel->get_context();

        $options = new stdClass();
        $options->area    = 'poasassignment_comment';
        $options->component    = 'mod_poasassignment';
        $options->pluginname = 'poasassignment';
        $options->context = $context;
        $options->autostart = true;
        $options->notoggle = true;
        $options->itemid  = $comments_itemid;

        $comment = new comment($options);
        $comment->set_post_permission(false);

        if ($comment->count() > 0)
            return $comment->output(true);
        else
            return null;
    }
}