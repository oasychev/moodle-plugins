<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_suspected_cheating\checkers\different_ip_checker;

defined('MOODLE_INTERNAL') || die();

class block_suspected_cheating_different_ip_checker_testcase extends advanced_testcase {
    /**
     * User works from different ip during the specified period of time
     * (from $config->from to $config->to).
     * It should report cheater activity.
     */
    public function test_it_should_detect_cheater() {
        $this->resetAfterTest();

        list($user) = static::prepare_database();

        static::insert_log_entry(['userid' => $user->id]);
        static::insert_log_entry(['userid' => $user->id, 'ip' => '127.0.0.2']);

        $checker = new different_ip_checker();
        $checker->set_config(static::get_config());
        $checker->check();

        $cheaters = static::get_cheaters();

        $this->assertEquals($user->id, $cheaters[0]->user_id);
    }

    /**
     * User works from different ip, but not during the specified period of time.
     * It should not report cheater activity.
     */
    public function test_it_should_not_detect_cheater_activity_which_was_made_not_in_specified_period_of_time() {
        $this->resetAfterTest();

        list($user) = static::prepare_database();

        static::insert_log_entry([
            'userid' => $user->id,
            'timecreated' => time() - 60 * 60 * 24,
        ]);
        static::insert_log_entry([
            'userid' => $user->id,
            'ip' => '127.0.0.2',
            'timecreated' => time() - 60 * 60 * 24,
        ]);

        $checker = new different_ip_checker();
        $checker->set_config(static::get_config());
        $checker->check();

        $cheaters = static::get_cheaters();

        $this->assertCount(0, $cheaters);
    }

    /**
     * User works from different ip, but suspicious activity is made with
     * interval that is bigger than the one specified in
     * $config->different_ip_checker_interval.
     * It should not report cheater activity.
     */
    public function test_it_should_not_detect_cheater_activity_if_specified_interval_is_smaller() {
        $this->resetAfterTest();

        list($user) = static::prepare_database();

        static::insert_log_entry([
            'userid' => $user->id,
            'timecreated' => time(),
        ]);
        static::insert_log_entry([
            'userid' => $user->id,
            'ip' => '127.0.0.2',
            'timecreated' => time() + 60 * 60,
        ]);

        $checker = new different_ip_checker();
        $checker->set_config(static::get_config());
        $checker->check();

        $cheaters = static::get_cheaters();

        $this->assertCount(0, $cheaters);
    }

    /**
     * User works from different ip, but different_ip_checker is disabled.
     * It should not report cheater activity.
     */
    public function test_it_should_not_detect_cheater_activity_if_rule_is_disabled() {
        $this->resetAfterTest();

        list($user) = static::prepare_database();

        static::insert_log_entry([
            'userid' => $user->id,
            'timecreated' => time(),
        ]);
        static::insert_log_entry([
            'userid' => $user->id,
            'ip' => '127.0.0.2',
            'timecreated' => time() + 60 * 60,
        ]);

        $checker = new different_ip_checker();
        $checker->set_config(static::get_config([
            'different_ip_checker_enabled' => false,
        ]));
        $checker->check();

        $cheaters = static::get_cheaters();

        $this->assertCount(0, $cheaters);
    }

    /**
     * User works from different ip, but in another course.
     * It should not report cheater activity.
     */
    public function test_it_should_not_detect_cheater_in_another_course() {
        $this->resetAfterTest();

        list($user1, $course1) = static::prepare_database();
        list($user2, $course2) = static::prepare_database();
        global $COURSE;
        $COURSE = $course1;

        static::insert_log_entry([
            'userid' => $user2->id,
            'timecreated' => time(),
        ]);
        static::insert_log_entry([
            'userid' => $user2->id,
            'ip' => '127.0.0.2',
            'timecreated' => time(),
        ]);

        $checker = new different_ip_checker();
        $checker->set_config(static::get_config());
        $checker->check();

        $cheaters = static::get_cheaters();

        $this->assertCount(0, $cheaters);
    }

    public function integration_with_supervised_block_provider() {
        return [
            ['127.0.0.2-10', 0],
            ['127.0.0.3-10', 1],
        ];
    }

    /**
     * User works from ip 127.0.0.1, then he change computer and works from ip
     * 127.0.0.2. Supervised block is enabled.
     * @dataProvider integration_with_supervised_block_provider
     */
    public function test_integration_with_supervised_block($iplist, $logscount) {
        $this->resetAfterTest();

        list($user) = static::prepare_database();
        static::enable_supervised_block($user, $iplist);

        static::insert_log_entry(['userid' => $user->id]);
        static::insert_log_entry(['userid' => $user->id, 'ip' => '127.0.0.2', 'timecreated' => time() + 1]);
        static::insert_log_entry(['userid' => $user->id, 'ip' => '127.0.0.2', 'timecreated' => time() + 2]);

        $checker = new different_ip_checker();
        $checker->set_config(static::get_config([
            'different_ip_checker_supervised_enabled' => true,
        ]));
        $checker->check();

        $cheaters = static::get_cheaters();

        $this->assertCount($logscount, $cheaters);
    }

    /**
     * Get config for checker.
     * @param array $params
     * @return object
     */
    protected static function get_config(array $params = []) {
        return (object)array_merge([
            'different_ip_checker_enabled' => true,
            'different_ip_checker_interval' => 60 * 15, // 15 minutes.
            'different_ip_checker_supervised_enabled' => false,
            'from' => time() - 60 * 60 * 3, // Now - 3 hours.
            'to' => time() + 60 * 60 * 3, // Now + 3 hours.
        ], $params);
    }

    /**
     * Insert record to log table.
     * @param array $params
     */
    protected static function insert_log_entry(array $params = []) {
        global $DB, $COURSE;

        $DB->insert_record(
            'logstore_standard_log',
            (object)array_merge([
                'edulevel' => 0,
                'contextid' => 1,
                'contextlevel' => 1,
                'contextinstanceid' => 1,
                'other' => '',
                'userid' => 1,
                'timecreated' => time(),
                'ip' => '127.0.0.1',
                'courseid' => $COURSE->id,
            ], $params)
        );
    }

    /**
     * Get cheaters.
     * @return array
     */
    protected static function get_cheaters() {
        global $DB;
        $cheaters = $DB->get_records('block_susp_cheat_differen_ip');

        return array_values($cheaters);
    }

    /**
     * Set up database.
     * @return array
     */
    protected static function prepare_database() {
        $gen = static::getDataGenerator();
        global $COURSE;
        $COURSE = $gen->create_course();
        $gen->create_block(
            'suspected_cheating',
            ['course' => $COURSE->id]
        );

        $user = $gen->create_user();
        $gen->enrol_user($user->id, $COURSE->id, 'student');
        static::setUser($user);

        return [$user, $COURSE];
    }

    /**
     * Enable supervised block at course.
     * @param stdClass $student
     * @param $iplist
     */
    protected static function enable_supervised_block(
        stdClass $student,
        $iplist
    ) {
        $gen = static::getDataGenerator();
        global $COURSE;
        $gen->create_block(
            'supervised',
            ['course' => $COURSE->id]
        );

        $teacher = $gen->create_user();
        $gen->enrol_user($teacher->id, $COURSE->id, 'teacher');

        $group = $gen->create_group(['courseid' => $COURSE->id]);
        $gen->create_group_member([
            'groupid' => $group->id,
            'userid' => $student->id,
        ]);

        global $DB;
        $classroom = $DB->insert_record('block_supervised_classroom', (object)[
            'iplist' => $iplist,
            'name' => 'name1',
        ]);
        $lessontype = $DB->insert_record('block_supervised_lessontype', (object)[
            'name' => 'lection1',
            'courseid' => $COURSE->id,
        ]);

        global $CFG;
        require_once("$CFG->dirroot/blocks/supervised/sessions/sessionstate.php");

        $session = $DB->insert_record('block_supervised_session', (object)[
            'courseid' => $COURSE->id,
            'classroomid' => $classroom,
            'groupid' => $group->id,
            'teacherid' => $teacher->id,
            'lessontypeid' => $lessontype,
            'state' => StateSession::ACTIVE,
            'iplist' => $iplist,
            'timestart' => time(),
            'timeend' => time() + 60 * 60 * 90,
            'duration' => 90,
        ]);

        $DB->insert_record('block_supervised_user', (object)[
            'sessionid' => $session,
            'userid' => $student->id,
        ]);
    }
}
