<?php
// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     block
 * @subpackage  suspected cheating
 * @author      Julia Satilina <satilina.julia@gmail.com>
 * @copyright   2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../../../config.php');
require_once('../../classes/checkers/cheating_checker.php');
require_once('../../classes/checkers/different_ip_checker.php');

echo 'start test...<br/>';
global $DB;
$res = $DB->delete_records_select('block_susp_cheat_differen_ip', 'course_id=1 and at1>=1514764820 and at1<=1514764820+60*60*3');

use block_suspected_cheating\checkers\different_ip_checker;

$test = new different_ip_checker();
$config = new \stdClass();
$config->different_ip_checker_enabled = true;
$config->different_ip_checker_supervised_enabled = true;
$config->different_ip_checker_interval = 60 * 15;
$config->from = 1514764820;
$config->to = 1514764820 + 60 * 60 * 3;
$test->set_config($config);
$starttime = time();
echo 'start time '.$starttime.'<br/>';
$test->check();
$endtime = time();
echo 'end time '.$endtime.'<br/>';
echo 'interval '.($endtime - $starttime).'<br/>';
echo 'end test';