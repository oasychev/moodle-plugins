<?php
// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package     block
 * @subpackage  suspected cheating
 * @author      Julia Satilina <satilina.julia@gmail.com>
 * @copyright   2014 Oleg Sychev, Volgograd State Technical University
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once('../../../../config.php');

global  $DB, $CFG;

echo "start generation of data<br/>";

$rec = new stdClass();
$rec->enrol = 'manual';
$rec->courseid = 1;
$rec->roleid = 5;
$rec->timecreated = 1520437843;
$rec->timemodified = 1520437843;
$enrolid = $DB->insert_record('enrol', $rec, true);
for ($i = 1; $i <= 100; $i++) {
    $newuser = new stdClass();
    $newuser->auth = 'manual';
    $newuser->confirmed = 1;
    $newuser->mnethostid = 1;
    $newuser->username = 'student_'.$i;
    $newuser->password = 'student_'.$i;
    $newuser->firstname = 'Name_'.$i;
    $newuser->lastname = 'SecondName_'.$i;
    $newuser->email = 'student_'.$i.'@mail.ru';
    $newuser->emailstop = 0;
    $newuser->lang = 'ru';
    $newuser->calendartype = 'gregorian';
    $newuser->timezone = 99;
    $newuser->timecreated = 1514764800;
    $newuser->timemodified = 1514764800;
    $userid = $DB->insert_record('user', $newuser, true);
    if ($i == 1) {
        $useridmin = $userid;
    } else if ($i == 100) {
        $useridmax = $userid;
    }

    $newuserenrol = new stdClass();
    $newuserenrol->status = 0;
    $newuserenrol->enrolid = $enrolid;
    $newuserenrol->userid = $userid;
    $newuserenrol->timestart = 1514764810;
    $newuserenrol->timecreated = 1514764810;
    $newuserenrol->timemodified = 1514764810;
    $result = $DB->insert_record('user_enrolments', $newuserenrol, false);
    $newroleassignments = new stdClass();
    $newroleassignments->roleid = 5;
    $newroleassignments->contextid = 1;
    $newroleassignments->userid = $userid;
    $newroleassignments->timemodified = 1514764810;
    $result = $DB->insert_record('role_assignments', $newroleassignments, false);
}

for ($i = 1; $i <= 100000; $i++) {
    $rdmip = random_int(1, 500);
    $rdmuserid = random_int($useridmin, $useridmax);
    $rdmtime = random_int(1, 60 * 60 * 3);
    $logrec = new stdClass();
    $logrec->eventname = 'block_suspected_cheating_test';
    $logrec->edulevel = 0;
    $logrec->contextid = 1;
    $logrec->contextlevel = 1;
    $logrec->contextinstanceid = 1;
    $logrec->other = '';
    $logrec->userid = $rdmuserid;
    $logrec->timecreated = 1514764820 + $rdmtime;
    $logrec->ip = '127.0.0.' . $rdmip;
    $logrec->courseid = 1;
    $result = $DB->insert_record('logstore_standard_log', $logrec, false);
}

echo "end generation of data<br/>";