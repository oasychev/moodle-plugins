<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_suspected_cheating\checkers;

use MoodleQuickForm;
use stdClass;

defined('MOODLE_INTERNAL') || die();

/**
 * Class cheating_checker.
 *
 * The base class of all cheating checkers.
 *
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class cheating_checker {
    /**
     * @var stdClass|null
     */
    protected $config;

    /**
     * Add any fields that checker requires to the suspected cheating block
     * settings form. This method is called from
     * suspected_cheating/edit_form.php.
     * @param MoodleQuickForm $form
     */
    public function define_settings_form_elements(MoodleQuickForm $form) {
    }

    /**
     * Check for suspicious users activity.
     */
    public abstract function check();

    /**
     * This method is called before suspected cheating block config is saved in
     * db. You can modify instance configuration data after user input before
     * it will be saved in db.
     * @param stdClass $data
     */
    public function pre_save_hook(stdClass $data) {
    }

    /**
     * @param stdClass|null $config
     */
    public function set_config($config) {
        $this->config = $config;
    }

    /**
     * Outputs information about cheaters activity.
     * @param array $params
     * @return void
     */
    public abstract function output(array $params = []);
}
