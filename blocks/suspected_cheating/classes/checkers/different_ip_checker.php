<?php
// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_suspected_cheating\checkers;

use coding_exception;
use context_course;
use dml_exception;
use flexible_table;
use HTML_QuickForm_Error;
use MoodleQuickForm;
use stdClass;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/tablelib.php');

/**
 * Class different_ip_checker.
 *
 * This class looks for users, which accessed site from different ip during
 * specified interval of time.
 *
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class different_ip_checker extends cheating_checker {
    /**
     * Seconds per minute.
     */
    const SECONDS_PER_MINUTE = 60;

    /**
     * {@inheritdoc}
     * @throws HTML_QuickForm_Error
     */
    public function define_settings_form_elements(MoodleQuickForm $form) {
        $form->addElement(
            'header',
            'configheader',
            get_string(
                'different_ip_checker_settings_header',
                'block_suspected_cheating'
            )
        );

        $form->addElement(
            'advcheckbox',
            'config_different_ip_checker_enabled',
            get_string(
                'different_ip_checker_settings_enabled',
                'block_suspected_cheating'
            )
        );
        $form->setDefault('config_different_ip_checker_enabled', true);

        $form->addElement(
            'duration',
            'config_different_ip_checker_interval',
            get_string(
                'different_ip_checker_settings_interval',
                'block_suspected_cheating'
            )
        );
        $form->setDefault(
            'config_different_ip_checker_interval',
            15 * self::SECONDS_PER_MINUTE
        );
        $form->setType(
            'config_different_ip_checker_interval',
            PARAM_INT
        );

        $form->addElement(
            'advcheckbox',
            'config_different_ip_checker_supervised_enabled',
            get_string(
                'different_ip_checker_settings_supervised_enabled',
                'block_suspected_cheating'
            )
        );
        $form->addHelpButton(
            'config_different_ip_checker_supervised_enabled',
            'different_ip_checker_settings_supervised_enabled',
            'block_suspected_cheating'
        );
    }

    /**
     * {@inheritdoc}
     * @throws dml_exception
     * @throws coding_exception
     */
    public function check() {
        if ($this->config === null
            || !$this->config->different_ip_checker_enabled
        ) {
            return;
        }

        global $COURSE;
        $usersid = get_enrolled_users(
            context_course::instance($COURSE->id),
            'moodle/grade:view' // Only students have this capability.
        );

        if (count($usersid) === 0) {
            return;
        }

        global $DB, $CFG;
        $from = $this->config->from;
        $to = $this->config->to;
        list($sqlin, $params) = $DB->get_in_or_equal(array_keys($usersid));
        $optimization = '';
        if ($CFG->dbtype == 'mysqli' || $CFG->dbtype == 'mysql') {
            $optimization = 'force INDEX({logsstanlog_tim_ix})';
        }
        $sql = "SELECT a.id, a.userid, a.ip, a.timecreated
                    FROM {logstore_standard_log} a ".$optimization.
                    " JOIN (
                        SELECT userid
                            FROM {logstore_standard_log}
                            WHERE userid $sqlin
                        GROUP BY userid
                        HAVING MIN(ip) <> MAX(ip)
                    ) b
                    USING (userid)
                    WHERE timecreated >= $from
                    AND timecreated <= $to
                ORDER BY timecreated";
        $logs = $DB->get_records_sql($sql, $params);

        $this->find_cheaters($logs);
    }

    /**
     * Find suspected cheaters and insert info about their activity into db.
     * @param array $logs
     * @throws dml_exception
     * @throws coding_exception
     */
    protected function find_cheaters(array $logs) {
        $groupedlogs = $this->group_logs_by_user($logs);

        $cheaters = [];
        foreach ($groupedlogs as $userlogs) {
            $cheater = $this->check_if_user_is_cheating($userlogs);

            if ($cheater) {
                $cheaters[] = $cheater;
            }
        }

        global $DB, $COURSE;
        $cheaters = array_map(function ($cheater) use ($COURSE) {
            return (object)[
                'user_id' => $cheater[0]->userid,
                'course_id' => $COURSE->id,
                'ip1' => $cheater[0]->ip,
                'ip2' => $cheater[1]->ip,
                'at1' => $cheater[0]->timecreated,
                'at2' => $cheater[1]->timecreated,
            ];
        }, $cheaters);

        $DB->insert_records('block_susp_cheat_differen_ip', $cheaters);
    }

    /**
     * Group logs by user.
     * @param array $logs
     * @return array
     */
    protected function group_logs_by_user(array $logs) {
        $groupedlogs = [];

        foreach ($logs as $log) {
            if (isset($groupedlogs[$log->userid])) {
                $groupedlogs[$log->userid][] = $log;
            } else {
                $groupedlogs[$log->userid] = [$log];
            }
        }

        return $groupedlogs;
    }

    /**
     * Return log rows with found suspicious activity if there are user activity
     * from different ip during time interval, specified in
     * $this->config->different_ip_checker_interval.
     * @param array $logs
     * @return array
     */
    protected function check_if_user_is_cheating(array $logs) {
        $interval = $this->config->different_ip_checker_interval;

        for ($i = 0, $len = count($logs); $i < $len; ++$i) {
            for ($j = $i + 1; $j < $len; ++$j) {
                $diff = abs($logs[$i]->timecreated - $logs[$j]->timecreated);
                $ipdifferent = $logs[$i]->ip !== $logs[$j]->ip;

                if ($ipdifferent && $interval >= $diff) {
                    $cond = $this->config->different_ip_checker_supervised_enabled
                        && static::all_log_entries_from_the_same_ip(array_slice($logs, $j))
                        && $this->check_active_user_sessions_in_supervised_block($logs[$j]);

                    if ($cond) {
                        break;
                    }

                    return [$logs[$i], $logs[$j]];
                }
            }
        }

        return [];
    }

    /**
     * Return true, if all log entries from the same ip.
     * @param array $logs
     * @return bool
     */
    protected static function all_log_entries_from_the_same_ip(array $logs) {
        $result = true;
        $firstip = count($logs) > 0 ? $logs[0]->ip : '';

        foreach ($logs as $log) {
            $result = $result && $log->ip === $firstip;

            if (!$result) {
                break;
            }
        }

        return $result;
    }

    /**
     * @param stdClass $log
     * @return bool
     */
    protected function check_active_user_sessions_in_supervised_block(
        stdClass $log
    ) {
        if (!$this->config->different_ip_checker_supervised_enabled) {
            return false;
        }

        global $CFG;
        $filename = "$CFG->dirroot/blocks/supervised/lib.php";
        if (!file_exists($filename)) {
            return false;
        }

        require_once($filename);

        global $USER;
        $USER = get_complete_user_data('id', $log->userid);
        $USER->lastip = $log->ip;

        $error = '';
        user_active_sessions($error);

        if ($error) {
            return false;
        }

        return true;
    }

    /**
     * @inheritdoc
     * @throws \dml_exception
     */
    public function output(array $params = []) {
        global $PAGE, $DB;

        echo '<h2>' . get_string(
            'different_ip_checker_table_header',
            'block_suspected_cheating'
        ) . '</h2>';

        $table = new flexible_table('block-suspected-cheating-different-ip');
        $table->baseurl = $PAGE->url;
        $table->define_columns(['user', 'ip1', 'ip2', 'at1', 'at2']);
        $table->define_headers([
            get_string('fullname', 'moodle'),
            get_string('different_ip_checker_ip1', 'block_suspected_cheating'),
            get_string('different_ip_checker_ip2', 'block_suspected_cheating'),
            get_string('different_ip_checker_at1', 'block_suspected_cheating'),
            get_string('different_ip_checker_at2', 'block_suspected_cheating'),
        ]);
        $table->collapsible(true);
        $table->initialbars(false);
        $table->set_attribute('width', '100%');
        $table->setup();

        $where = '';
        if (isset($params['from'], $params['to'])) {
            $from = $params['from'];
            $to = $params['to'];

            $where = "WHERE at1>=$from AND at1<=$to";
        }

        $data = $DB->get_records_sql(
            "SELECT b.*, u.firstname, u.lastname, u.firstnamephonetic,
                    u.lastnamephonetic, u.middlename, u.alternatename
               FROM {user} u
               JOIN {block_susp_cheat_differen_ip} b
                 ON b.user_id=u.id
              $where"
        );

        $user = new stdClass();
        foreach ($data as $row) {
            $user->firstname = $row->firstname;
            $user->lastname = $row->lastname;
            $user->firstnamephonetic = $row->firstnamephonetic;
            $user->lastnamephonetic = $row->lastnamephonetic;
            $user->middlename = $row->middlename;
            $user->alternatename = $row->alternatename;

            $table->add_data_keyed([
                'user' => fullname($user),
                'ip1' => $row->ip1,
                'ip2' => $row->ip2,
                'at1' => date('Y-m-d I:h:s', $row->at1),
                'at2' => date('Y-m-d I:h:s', $row->at2),
            ]);
        }

        $table->finish_output();
    }
}
