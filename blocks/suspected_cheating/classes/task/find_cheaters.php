<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace block_suspected_cheating\task;

use block_suspected_cheating\checkers\different_ip_checker;
use context;
use core\task\scheduled_task;

defined('MOODLE_INTERNAL') || die();

/**
 * Class find_cheaters.
 *
 * Class that runs find cheaters by cron.
 *
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class find_cheaters extends scheduled_task {
    /**
     * Array of checkers.
     * @var array
     */
    protected $checkers = [];

    public function __construct() {
        $this->checkers[] = new different_ip_checker();
    }

    /**
     * @inheritdoc
     */
    public function get_name() {
        return get_string('find_cheaters', 'block_suspected_cheating');
    }

    /**
     * @inheritdoc
     * @throws \dml_exception
     */
    public function execute() {
        global $COURSE, $DB;

        $blockname = 'suspected_cheating';
        $instances = $DB->get_records(
            'block_instances',
            ['blockname' => $blockname]
        );

        $lastruntime = $this->get_last_run_time();

        foreach ($instances as $instance) {
            $block = block_instance($blockname, $instance);

            $config = $block->config;
            if ($config === null
                || !isset($config->to)
                || $config->to < time()
            ) {
                continue;
            }

            $context = context::instance_by_id($instance->parentcontextid);
            $COURSE = get_course($context->instanceid);

            if ($config->from < $lastruntime) {
                $config->from = $lastruntime;
            }

            foreach ($this->checkers as $checker) {
                $checker->set_config($config);
                $checker->check();
            }
        }
    }
}
