<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use block_suspected_cheating\checkers\cheating_checker;
use block_suspected_cheating\checkers\different_ip_checker;

defined('MOODLE_INTERNAL') || die();

/**
 * Class block_suspected_cheating
 *
 * The main idea of suspected cheating block is to monitor student activity and
 * show to teacher information about suspicious one based on different rules.
 *
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_suspected_cheating extends block_base {
    /**
     * Cheating checkers.
     *
     * @var cheating_checker[]
     */
    protected $checkers = [];

    /**
     * @inheritdoc
     */
    public function init() {
        $this->title = get_string(
            'suspected_cheating',
            'block_suspected_cheating'
        );

        $this->checkers[] = new different_ip_checker();
    }

    /**
     * @inheritdoc
     * @throws \moodle_exception
     */
    public function get_content() {
        if (!has_capability('block/suspected_cheating:view', $this->context)) {
            return '';
        }

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;

        global $COURSE;
        $url = new moodle_url('/blocks/suspected_cheating/view.php', [
            'blockid' => $this->instance->id,
            'courseid' => $COURSE->id,
        ]);
        $this->content->footer = html_writer::link(
            $url,
            get_string('more', 'block_suspected_cheating')
        );

        if ($this->config === null) {
            $this->content->text = get_string(
                'block_is_not_configured',
                'block_suspected_cheating'
            );
            return $this->content;
        } else if ($this->config->to < time()) {
            $this->content->text = get_string(
                'block_is_not_active',
                'block_suspected_cheating'
            );
            return $this->content;
        }

        return $this->content;
    }

    /**
     * @inheritdoc
     */
    public function applicable_formats() {
        return [
            'course-view' => true,
        ];
    }

    /**
     * @inheritdoc
     */
    public function instance_config_save($data, $nolongerused = false) {
        foreach ($this->checkers as $checker) {
            $checker->pre_save_hook($data);
        }

        parent::instance_config_save($data, $nolongerused);
    }

    /**
     * @return cheating_checker[]
     */
    public function get_checkers() {
        return $this->checkers;
    }
}
