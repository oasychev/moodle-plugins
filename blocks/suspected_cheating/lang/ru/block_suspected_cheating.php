<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Блок подозрительной активности';
$string['suspected_cheating'] = 'Подозрительная активность';

$string['suspected_cheating:addinstance'] = 'Добавить новый блок подозрительной активности';
$string['suspected_cheating:viewcheaters'] = 'Просматривать подозрительную активность';

// Cron task name.
$string['find_cheaters'] = 'Найти жуликов';

$string['suspected_cheating_settings_header'] = 'Настройки блока подозрительной активности';
$string['suspected_cheating_settings_from'] = 'С';
$string['suspected_cheating_settings_to'] = 'До';

$string['block_is_not_configured'] = 'Блок не настроен. Включите режим редактирования и настройте его.';
$string['block_is_not_active'] = 'Блок не активен. Задайте новый период в настройках блока';

$string['more'] = 'Подробнее';

$string['filter'] = 'Фильтровать';
$string['filter_header'] = 'Фильтровать записи по времени';

// Different ip checker.
$string['different_ip_checker_settings_header'] = 'Настройки правила работы с разных ip';
$string['different_ip_checker_settings_interval'] = 'Интервал';
$string['different_ip_checker_settings_enabled'] = 'Включено';
$string['different_ip_checker_settings_supervised_enabled'] = 'Интеграция с блоком supervised';
$string['different_ip_checker_settings_supervised_enabled_help'] = 'Будут учитываться активные сессии пользователя в блоке supervised';

$string['different_ip_checker_table_header'] = 'Заходы с разных ip';

$string['different_ip_checker_ip1'] = 'IP первого захода';
$string['different_ip_checker_ip2'] = 'IP второго захода';
$string['different_ip_checker_at1'] = 'Время первого захода';
$string['different_ip_checker_at2'] = 'Время второго захода';
