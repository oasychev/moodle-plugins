<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Suspected cheating block';
$string['suspected_cheating'] = 'Suspected cheating';

$string['suspected_cheating:addinstance'] = 'Add a new suspected cheating block';
$string['suspected_cheating:viewcheaters'] = 'View cheaters activity';

// Cron task name.
$string['find_cheaters'] = 'Find cheaters';

$string['suspected_cheating_settings_header'] = 'Suspected cheating block settings';
$string['suspected_cheating_settings_from'] = 'From';
$string['suspected_cheating_settings_to'] = 'To';

$string['block_is_not_configured'] = 'Block is not configured. Turn editing on and configure it.';
$string['block_is_not_active'] = 'Block is not active. Configure new period in block settings.';

$string['more'] = 'More';

$string['filter'] = 'Filter';
$string['filter_header'] = 'Filter records by time';

// Different ip checker.
$string['different_ip_checker_settings_header'] = 'Different ip checker rule settings';
$string['different_ip_checker_settings_interval'] = 'Interval';
$string['different_ip_checker_settings_enabled'] = 'Enabled';
$string['different_ip_checker_settings_supervised_enabled'] = 'Integration with supervised block';
$string['different_ip_checker_settings_supervised_enabled_help'] = 'Active user sessions in supervised block will be considered';

$string['different_ip_checker_table_header'] = 'Different ip rule';

$string['different_ip_checker_ip1'] = 'First access IP';
$string['different_ip_checker_ip2'] = 'Second access IP';
$string['different_ip_checker_at1'] = 'Time of first access';
$string['different_ip_checker_at2'] = 'Time of second access';
