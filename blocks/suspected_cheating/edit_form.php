<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Class block_suspected_cheating_edit_form
 *
 * Class for edit block settings.
 *
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_suspected_cheating_edit_form extends block_edit_form {
    /**
     * Seconds per minute.
     */
    const SECOND_PER_MINUTE = 60;

    /**
     * @param MoodleQuickForm $mform
     * @throws \HTML_QuickForm_Error
     */
    protected function specific_definition($mform) {
        $mform->addElement(
            'header',
            'config_header',
            get_string(
                'suspected_cheating_settings_header',
                'block_suspected_cheating'
            )
        );

        $mform->addElement(
            'date_time_selector',
            'config_from',
            get_string(
                'suspected_cheating_settings_from',
                'block_suspected_cheating'
            )
        );
        $mform->setDefault('config_from', time());
        $mform->setType('config_from', PARAM_INT);

        $mform->addElement(
            'date_time_selector',
            'config_to',
            get_string(
                'suspected_cheating_settings_to',
                'block_suspected_cheating'
            )
        );
        $mform->setDefault('config_to', time() + 150 * self::SECOND_PER_MINUTE);
        $mform->setType('config_to', PARAM_INT);

        foreach ($this->block->get_checkers() as $checker) {
            $checker->define_settings_form_elements($mform);
        }
    }
}
