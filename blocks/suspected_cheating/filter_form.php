<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once("$CFG->libdir/formslib.php");

/**
 * Class suspected_cheating_filter_form
 *
 * Form for filter records about cheaters activity by time.
 *
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class suspected_cheating_filter_form extends moodleform {
    public function definition() {
        $mform = $this->_form;

        $mform->addElement(
            'header',
            'filterheader',
            get_string('filter_header', 'block_suspected_cheating')
        );

        $mform->addElement(
            'date_time_selector',
            'from',
            get_string(
                'suspected_cheating_settings_from',
                'block_suspected_cheating'
            )
        );
        $mform->setDefault('from', time());
        $mform->setType('from', PARAM_INT);

        $mform->addElement(
            'date_time_selector',
            'to',
            get_string(
                'suspected_cheating_settings_to',
                'block_suspected_cheating'
            )
        );
        $mform->setDefault('to', time() + 150 * 60);
        $mform->setType('to', PARAM_INT);

        $mform->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'blockid', $this->_customdata['blockid']);
        $mform->setType('blockid', PARAM_INT);

        $buttonarray = [];
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string(
            'filter',
            'block_suspected_cheating'
        ));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', [' '], false);
        $mform->closeHeaderBefore('buttonar');
    }
}
