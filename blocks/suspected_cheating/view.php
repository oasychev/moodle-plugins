<?php

// This file is part of Student Access Control Kit - https://bitbucket.org/oasychev/moodle-plugins/overview
//
// Student Access Control Kit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Student Access Control Kit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    block
 * @subpackage suspected_cheating
 * @author     Alexander Melihov <amelihovv@ya.ru>
 * @copyright  2017 Oleg Sychev, Volgograd State Technical University
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('./filter_form.php');

global $CFG, $DB, $PAGE, $OUTPUT;

$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);

if (!$course = $DB->get_record('course', ['id' => $courseid])) {
    print_error('invalidcourse', 'block_suspected_cheating', $courseid);
}

require_login($course);

$blockurl = new moodle_url('/blocks/suspected_cheating/view.php', [
    'courseid' => $courseid,
    'blockid' => $blockid,
]);

$PAGE->set_url('/blocks/suspected_cheating/view.php', ['id' => $courseid]);
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('suspected_cheating', 'block_suspected_cheating'));

require_capability('block/suspected_cheating:viewcheaters', $PAGE->context);

$settingsnode = $PAGE->settingsnav->add(
    get_string('suspected_cheating', 'block_suspected_cheating')
);
$blocknode = $settingsnode->add(
    get_string('suspected_cheating', 'block_suspected_cheating'),
    $blockurl
);
$blocknode->make_active();

echo $OUTPUT->header();

$mform = new suspected_cheating_filter_form(null, [
    'courseid' => $courseid,
    'blockid' => $blockid,
]);
echo $mform->render();

$params = [];
if (!$mform->is_cancelled() && $data = $mform->get_data()) {
    $params = $data;
}

$instance = $DB->get_record('block_instances', ['id' => $blockid]);
$block = block_instance('suspected_cheating', $instance);

foreach ($block->get_checkers() as $checker) {
    $checker->output($params);
}

echo $OUTPUT->footer();
