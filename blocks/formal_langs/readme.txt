This is a technical block providing natural- and formal-languages processing features. The only side visible to the user is managing the list of languages accessible in the particular course.

Simple English
 - tokenization
 - typo detection (using Damerau-Levenshein distance: insertions, deletions, modifications, transpositions) and generating a picture showing how to fix typos

Programming languages C and C++
 - tokenization
 - parsing (beta)